import React, { Component } from "react";
import { Root } from "native-base";
import { AppLoading } from "expo";
import * as Font from 'expo-font';
import {
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import { NativeRouter } from 'react-router-native';
import Routes from './Routes';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }
  render() {
    if (this.state.loading) {
      return (
        <Root>
          <AppLoading />
        </Root>
      );
    }
    return (
      <NativeRouter>
        <Routes/>
      </NativeRouter>
    );
  }
}

export default App;
