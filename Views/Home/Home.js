import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { H3, Button, Text, Picker, Icon } from 'native-base';
import { Col, Grid } from 'react-native-easy-grid';
const COL_PADDING = 16;
const TEXT_PADDING = 16;
const styles = StyleSheet.create({
    root: {
        alignItems: "center",
        paddingTop: 32
    },
    col: {
        padding: COL_PADDING
    },
    text: {
        paddingTop: 16,
        textAlign: "center"
    },
    button: {
        paddingTop: 32,
        padding: 16,
    },
    picker: {
        padding: 32,
        paddingBottom:0
    }
});


const Home = () => {
    const [selected, setSelected] = useState(0);

    const handleOnValueChange = (value) => {
        setSelected(value);
    }

    return (
        <>
            <Grid style={styles.root}>
                <Col style={styles.col}>
                    <H3 style={styles.text}>How do you want to send SMS?</H3>
                    <View style={styles.picker}>
                        <Picker
                            mode="dropdown"
                            iosHeader="Select Your Choice"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            selectedValue={selected}
                            onValueChange={handleOnValueChange}

                        >
                            <Picker.Item label="--Select--" value={0} />
                            <Picker.Item label="Upload Phone List" value={1} />
                            <Picker.Item label="Type Manually" value={2} />
                        </Picker>
                    </View>
                    <View style={styles.button}>
                        <Button full rounded>
                            <Text> Go </Text>
                        </Button>
                    </View>

                </Col>
            </Grid>

        </>
    );
}

export default Home;
