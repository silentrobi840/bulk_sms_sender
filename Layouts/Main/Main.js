import React from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Right,
    Body,
    Text
} from 'native-base';

const Main = (props) => {
    return (
        <Container>
            <Header>
                <Body>
                    <Title>Bulk SMS Sender</Title>
                </Body>
                <Right />
            </Header>
            <Content>
                {props.children}
            </Content>
            <Footer>
                <FooterTab>
                    <Button full>
                        <Text>Footer</Text>
                    </Button>
                </FooterTab>
            </Footer>
        </Container>
    );
}

export default Main;