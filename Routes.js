import React from 'react';
import { Switch, Redirect } from 'react-router-native';

import { RouteWithLayout } from './Components';
import { Main as MainLayout } from './Layouts';

import {
  Home as HomeView
} from './Views';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/home"
      />
      <RouteWithLayout
        component={HomeView}
        exact
        layout={MainLayout}
        path="/home"
      />
    </Switch>
  );
};

export default Routes;
